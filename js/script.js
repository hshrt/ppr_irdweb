var serverIp = "202.181.241.56";
var test_phk_serverIp = "210.176.163.123"
var serverPath = "/"+local+"/api/ird";
var logout_serverPath = "/"+local+"/api";
var audioFile = "media/Ding-small-bell.mp3";
	
var Order = Backbone.Model.extend({
	getRequirement : function () {
		var requirementText = '';

		if (this.get('requirement') != undefined) {
			requirementText = this.get('requirement');
		}
		return requirementText;
	},
	
	getStatus : function () {
		return this.get('status');
	},
	getId : function () {
		return this.get('id');
	},
	getRoom : function () {
		return this.get('room');
	},
	getGuestName : function () {
		return this.get('guestname');
	},	
	getDeliveryDate : function () {
		return this.get('deliveryTime').split(" ")[0];
	},
	getDeliveryTime : function () {
		return this.get('deliveryTime').split(" ")[1];
	},
	getOrderDate : function () {
		return this.get('orderTime').split(" ")[0];
	},
	getOrderTime : function () {
		return this.get('orderTime').split(" ")[1];
	},
	getNumOfGuest : function () {
		return this.get('numOfGuest');
	},
	getQuantity : function () {
		return this.get('quantity');
	},
	getItem : function () {
		return this.get('item');
	},
	getChoices : function () {
		var temp = this.get('choices');
		if (!temp) return temp;
		else return temp.split(", ").join("\n");
	},
	getStatusTxt : function () {
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "waiting for process";
				break;
			case '1':
				return "processed";
				break;
			case '2':
				return "cancelled";
				break;
		}
	},
	getDisplayPendOrderBtn : function (){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "inline";
				break;
			case '1':
				return "none";
				break;
			case '2':
				return "none";
				break;
		}
	},
	getDisplayCanOrderBtn : function(){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "none";
				break;
			case '1':
				return "inline";
				break;
			case '2':
				return "none";
				break;
		}
	},
	getLastUpdateTime: function(){
		return this.get('lastUpdate');
	},
	getLastUpdateBy: function (){
		return this.get('lastUpdateBy');
	},
	getTxtColor: function(){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "color:red";
				break;
			case '1':
				return "color:green";
				break;
			case '2':
				return "color:black";
				break;
		}
	},
	getPoOn: function(){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "poOn";
				break;
			case '1':
				return "";
				break;
			case '2':
				return "";
				break;
		}
	}
});

var Orders = Backbone.Collection.extend({

	model : Order,
	searchDate : false,
	searchStatus : false,
	searchRoom : false,
	
	url : function () {
		console.log("url");
		//-- test --//
		//return 'http://'+test_phk_serverIp+serverPath+'/getOrder.php?' + 'date=' + encodeURIComponent(this.searchDate) + '&status=' + encodeURIComponent(this.searchStatus) + '&room=' + encodeURIComponent(this.searchRoom);
		
		return '..'+serverPath+'/getOrder.php?' + 'date=' + encodeURIComponent(this.searchDate) + '&status=' + encodeURIComponent(this.searchStatus) + '&room=' + encodeURIComponent(this.searchRoom);
	},
	parse : function (response) {
		console.log("parse: "+response.data);
		setUpdateTime();
		return response.data;
	},
	searchFor : function (date, status, room) {
		console.log("searchFor: "+'date=' + encodeURIComponent(date) + '&status=' + encodeURIComponent(status) + '&room=' + encodeURIComponent(room));
		if (date==null || date == 0 || !checkDateString(date)) date = getTodayDate();
		if (status==null) status="";
		if (room==null || room == 0) room="";
		this.searchDate = date;
		this.searchStatus = status;
		this.searchRoom = room;
		this.fetch();
		return this;
	}
});

var OrdersList = Backbone.View.extend({
	tagName: 'ul',
	id: 'results',
	className: 'list-group',
	template: function () { return ''; },
	initialize: function(options) {
		console.log("OrdersList init");
		this.listenTo(this.collection, 'sync', this.render);
		if (options.template) {
			this.template = options.template;
		}
	},
	render: function () {
		console.log("OrdersList render ");
		var templateData = {
			results: this.collection.map(this._generateRowData)
		};
		var html = this.template(templateData);
		this.$el.html(html);
		$(window).scrollReadPos();
		if ($(".poOn").length>0){
			$(".poOn").effect("shake");
			playSound();
		}
		
		return this;
	},
	_generateRowData: function (model) {
		console.log("The requirement is " + model.getRequirement());
		return {
			id:           		model.getId(),
			status:       		model.getStatus(),
			room:         		model.getRoom() + " (" + model.getGuestName() + ")",
			deliveryDate: 		model.getDeliveryDate(),
			deliveryTime: 		model.getDeliveryTime(),
			orderDate:    		model.getOrderDate(),
			orderTime:    		model.getOrderTime(),
			numOfGuest:   		model.getNumOfGuest(),
			quantity:     		model.getQuantity(),
			item:         		model.getItem(),
			choices:      		model.getChoices(),
			statusTxt:	  		model.getStatusTxt(),
			pendingBtnDisplay:  model.getDisplayPendOrderBtn(),
			cancelBtnDisplay: 	model.getDisplayCanOrderBtn(),
			lastUpdateTime:     model.getLastUpdateTime(),
			lastUpdateBy:       model.getLastUpdateBy(),
			txtColor:           model.getTxtColor(),
			poOn:               model.getPoOn(),
			requirement:        model.getRequirement()
		};
	},
	events: {
		'click .pending-order': 'pendingOrder',
		'click .update-button' : 'updateButtonClicked',
		'click .cancel': 'cancel'
	},
	pendingOrder: function(ev) {
		var id = $(ev.target).parents('li').attr('data-id');
		var status = $(ev.target).parents('li').attr('data-status');
		console.log("id: "+id+" s: "+status);
		$(ev.target).hide();
		switch(status){
			case "0":
				this.$('.process-order-'+id).show();
				this.$('.cancel-order-'+id).show();
				this.$('.unprocess-order-'+id).hide();
				break;
			case "1":
				this.$('.process-order-'+id).hide();
				this.$('.cancel-order-'+id).show();
				break;
			case "2":
				break;
			default:
		}
	},
	cancel: function() {
		resultView.render();
	}
	
});

var SearchForm = Backbone.View.extend({
	initialize: function () {
		if (this.collection) {
			this.listenTo(this.collection, 'sync', this.render);
		}
		console.log("order date: "+this.collection.searchDate);
		
		//this.$("#order_date").change(function(){$("#order_date").trigger('submit')});
		this.$("#status").change(function(){$("#order_date").trigger('submit')});
		this.$("#room").change(function(){$("#order_date").trigger('submit')});
	},
	render: function () {
		this.$("#order_date").val(this.collection.searchDate);
		this.$("#status").val(this.collection.searchStatus);
		this.$("#room").val(this.collection.searchRoom);
	},
	events: {
		'submit': function (ev) {
			ev.preventDefault();
			var date = this.$("#order_date").val();
			var status = this.$("#status").val();
			if (status=="-1")status="";
			var room = this.$("#room").val();
			
			console.log("submit");
			console.log(date+","+status+","+room);
			
			this.trigger('search', date, status, room);
		}
	}
});

var PageRouter = Backbone.Router.extend({
	routes: {
		'search/(:date)+(:status)+(:room)':  'search'
	}
});

var searchResults = new Orders();

var resultView = new OrdersList({
	el: '#results',
	collection: searchResults,
	template: Handlebars.compile($('#order-list-template').html())
});


var router = new PageRouter();


var searchView = new SearchForm({
	el: '#search',
	collection: searchResults
});

var newDate=null, newStatus="0", newRoom=null;

router.on('route:search', function (date, status, room) {
	console.log("router.");
	searchResults.searchFor(date, status, room);
	newDate=date;
	newStatus=status;
	newRoom=room;
});

searchView.on('search', function (date, status, room) {
	console.log("searchView");
    if (!checkDateString(date)) date = date.substring(0, 10);
	router.navigate("search/"+encodeURIComponent(date)+"+"+encodeURIComponent(status)+"+"+encodeURIComponent(room), {trigger:true});
});
	
	Backbone.history.start();
	
$.fn.scrollSetPos = function(){
	console.log("scrollSetPos");
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        $(window).scroll(function(e) {
            localStorage["posStorage"] = $(window).scrollTop();
			console.log("scrollSetPos: "+ localStorage["posStorage"]);
        });

        return true;
    }

    return false;
};

$.fn.scrollReadPos = function(){
	console.log("scrollReadPos");
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        if (posReader) {
			console.log("scrollReadPos: "+posReader);
            $(window).scrollTop(posReader);
            localStorage.removeItem("posStorage");
        }
        return true;
    }

    return false;
};
    

$( document ).ready( function() {
	console.log("document ready");
	$(window).scrollSetPos();
	window.setInterval(function(){
			 checkMintues();
             console.log("refreshing"+this.getCurrentTime());
			 searchResults.searchFor(newDate, newStatus, newRoom);
		 }, 10000);
});

function checkMintues(){
    console.log("mintues");
    var date = new Date();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    console.log(min);
    var hash = window.location.hash;
    console.log(hash);
    
    if (min == 0 && sec<10){
        console.log("reload");
        if (hash){
            window.location.hash = '';
        }
        window.location.reload();
    }
}

function SubForm (id, status){
	console.log(id+"+"+status);
    $.ajax({
		//--test link--
        //url:'http://'+test_phk_serverIp+serverPath+'/updateOrder.php?orderId='+id+'&status='+status,
		url:'..'+serverPath+'/updateOrder.php?orderId='+id+'&status='+status,
        type:'post',
        success:function(){
            alert("Order Processed!");
			window.location.reload();
        },
		error: function(){
			alert("Server is busy at the momnet, please try again later.")
			window.location.reload();
		}
    });
};

function getTodayDate(){
	console.log("todayDate");
	var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = yyyy+'-'+mm+'-'+dd;
		return today;
}

function setUpdateTime(){
    document.getElementById('update_time').innerHTML="As of " + this.getCurrentTime();
};

function getCurrentTime() {
    var dt = new Date();
    var minute = dt.getMinutes();
    var second = dt.getSeconds();
    var minuteFormatted = minute < 10 ? "0" + minute : minute;
    var secondFormatted = second < 10 ? "0" + second : second;
    var t = dt.getHours() + ":" + minuteFormatted + ":" + secondFormatted;
    return t;
}



function checkDateString(dateString){
    var regex = new RegExp("[0-9]{4}-[0-9]{2}-[0-9]{2}$");
    return regex.test(dateString);
}

function sessionEmail(){
	var msg = "Welcome, "+this.getSessionEmail();
	document.getElementById('topMsg').innerHTML=msg;
}

function todayDate(){
	document.getElementById('order_date').value=this.getTodayDate();
};

function checkLogin(){
	console.log("checkLogin");
	if (sessionStorage.getItem('status') != 'loggedIn'){
    //redirect to page
		window.location.replace("login.html");
	}
	else{
    //show validation message
		return false;
	}
};

function getSessionEmail(){
	return sessionStorage.getItem('email');
}

function logout(){
		$.ajax({
			url:'..'+logout_serverPath+'/logout.php',
			type:'post',
			success:function(){
				window.location.replace("login.html");
				sessionStorage.removeItem('status');
				sessionStorage.removeItem('email');
				sessionStorage.removeItem('fail');
			},
			error: function(){
				window.location.reload();
			}
		});
	};
	
function playSound(){
	console.log("playSound");
	new Audio(audioFile).play();
    
};

function checkUserAgent() {
    var x = navigator.userAgent;
    console.log(x);
    return x.includes("Android 3");
};

/*--This JavaScript method for Print Preview command--*/
function PrintPreview(id) {
    var toPrint = document.getElementById('printarea-'+id);
    var popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write('<html><title>::Print Preview::</title><link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css"><link rel="stylesheet" type="text/css" href="js/print.css" media="screen"/></head><body><div class="container"><h1>In Room Dining Order</h1><li>')
    popupWin.document.write(toPrint.innerHTML);
    popupWin.document.write('<button class="btn btn-info" onclick="myFunction()">Print</button><script>function myFunction() {window.print();}</script></html>');
    popupWin.document.close();
};
