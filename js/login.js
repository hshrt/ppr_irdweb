$(function(){
    
	var serverIp = "192.168.15.114";
    var test_phk_serverIp = "210.176.163.123"
	var serverPath = "/"+local+"/api";
	
	
    LoginView = Backbone.View.extend({
        initialize: function() {
            _.bindAll(this, "render", "login_press")
            this.login = $("#login-template").html();
        },
        
        events: {
            "click .login_button":  "login_press"
        },
        
        render: function() {
            $(this.el).html(this.login);
            return this;
        },
        
        login_press: function(e) {
            e.preventDefault();
            $("#login-container").fadeOut();
            login(this.$("#email").val(), this.$("#pwd").val());
        }
        
    })
    
    
    
    
    var loginView = new LoginView();
    
    
    $("#main").append(loginView.render().el);
    
    function login(email, pwd){
		$.ajax({
			//----test----//
			//url:'http://'+test_phk_serverIp+serverPath+'/login.php',
			url:'..'+serverPath+'/login.php',
			type:'post',
			dataType:'json',
			data: {"email":email,
					"pwd":pwd},
			success:function(data){
				//alert(data);
				if (data['status'] == '1'){
					//alert("login success!");
					sessionStorage.setItem('status','loggedIn');
					sessionStorage.setItem('email',email);
					window.location.replace("index.html");
				}
				else {
					//alert("login fail!");
					sessionStorage.setItem('fail','1');
					window.location.reload();
				}
			},
			error: function(){
				//alert("Server is busy at the momnet, please try again later.")
				sessionStorage.setItem('fail','2');
				window.location.reload();
			}
		});
	};
    
	
    
});

function checkLoginFailed(){
		if (sessionStorage.getItem('fail')=='1'){
			$('#email_grp').addClass('has-error has-feedback');
			$('#pwd_grp').addClass('has-error has-feedback');
			$('#helpBlock').removeClass('invisible');
			$('#helpBlock').text('Invalid username/password, please try again.');
			sessionStorage.removeItem('fail');
		}else if(sessionStorage.getItem('fail')=='2'){
			$('#helpBlock').removeClass('invisible');
			$('#helpBlock').text('Server is busy at the momnet, please try again later.');
			sessionStorage.removeItem('fail');
		}
		
	};
